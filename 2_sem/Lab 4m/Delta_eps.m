clc, clear, close all

f = @(x, y) -(2.*y+x.^5.*y^3.*exp(x))./x;
ft = @(x) 1./(2.*x.^4.*exp(x)).^(1/2);
y0 = 1/sqrt(2*exp(1));
a = 1;
b = 2;

Err1 = zeros(1, 5);
Err2 = zeros(1, 5);
Err3 = zeros(1, 5);
Err4 = zeros(1, 5);
Err5 = zeros(1, 5);
Err6 = zeros(1, 5);
Err7 = zeros(1, 5);

I1 = zeros(1, 5);
I2 = zeros(1, 5);
I3 = zeros(1, 5);
I4 = zeros(1, 5);
I5 = zeros(1, 5);
I6 = zeros(1, 5);
I7 = zeros(1, 5);
    
Eps = zeros(1, 5);

for i = 1:5
    
    eps = 10^(-(i+1));
    opts = odeset('RelTol', eps, 'AbsTol', eps);
    
    [t1, y1] = ode23(f, [a b], y0, opts);
    [t2, y2] = ode45(f, [a b], y0, opts);
    [t3, y3] = ode113(f, [a b], y0, opts);
    [t4, y4] = ode15s(f, [a b], y0, opts);
    [t5, y5] = ode23s(f, [a b], y0, opts);
    [t6, y6] = ode23t(f, [a b], y0, opts);
    [t7, y7] = ode23tb(f, [a b], y0, opts);
    Err1(i) = max(abs(ft(t1)-y1));
    Err2(i) = max(abs(ft(t2)-y2));
  Err3(i) = max(abs(ft(t3)-y3));
    Err4(i) = max(abs(ft(t4)-y4));
    Err5(i) = max(abs(ft(t5)-y5));
    Err6(i) = max(abs(ft(t6)-y6));
    Err7(i) = max(abs(ft(t7)-y7));
    
    I1(i) = length(t1);
    I2(i) = length(t2);
   I3(i) = length(t3);
    I4(i) = length(t4);
    I5(i) = length(t5);
    I6(i) = length(t6);
    I7(i) = length(t7);
    
    Eps(i) = eps;
end

figure
loglog(Eps, Err1, Eps, Err2, Eps,Err3, Eps, Err4, Eps, Err5, Eps, Err6, Eps, Err7)
xlabel('eps');
ylabel('err');
title('График фактической точности от заданной точности');
legend('ode23', 'ode45','ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb', 'Location', 'NorthWest');
grid minor

figure
loglog(Eps, I1, Eps, I2, Eps, I3, Eps, I4, Eps, I5, Eps, I6, Eps, I7)
xlabel('eps');
ylabel('err');
title('График числа итераций от заданной точности');
legend('ode23', 'ode45','ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb');
grid minor