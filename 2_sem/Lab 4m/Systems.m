clc, clear, close all

y0 = [1; 0; 1; 0; 1; 0; 1; 0; 1; 0];
S = linspace(1e2, 1e6, 20);
a = 0;
b = 1;

E1 = {};
E2 = {};

for i = 1:10
    
    h = 0.1;
    
    Err1 = zeros(1, 20);
    Err2 = zeros(1, 20);
    
    for j = 1:20
        
        D = linspace(-1, -S(i), 10);
        D = diag(D);
        V = rand(10);
        A = V*D*V^(-1);
        
        f = @(x, y) A*y;
        tspan = a:h:b;
        [X, Y] = ode15s(f, tspan, y0);
      
        [t1, y1] = EulerES(A, a, b, y0, h);
        [t2, y2] = EulerIS(A, a, b, y0, h);

        Err1(j) = norm(Y - y1');
        Err2(j) = norm(Y - y2');
        
    end
   
    E1{i} = Err1;
    E2{i} = Err2;
    
    h = h - 0.01;
    
end   

figure 
loglog(S, E2{1}, S, E2{2}, S, E2{3}, S, E2{4}, S, E2{5}, S, E2{6}, S, E2{7}, S, E2{8}, S, E2{9}, S, E2{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости');
legend('h = 0.1', 'h = 0.09', 'h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure 
loglog(S, E1{1}, S, E1{2}, S, E1{3}, S, E1{4}, S, E1{5}, S, E1{6}, S, E1{7}, S, E1{8}, S, E1{9}, S, E1{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости');
legend('h = 0.1', 'h = 0.09', 'h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'SouthEast')
grid minor