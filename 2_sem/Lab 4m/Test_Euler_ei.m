clc, clear, close all

f = @(x, y) -100*y + 10;
ft = @(x) 1/10 + 9*exp(-100*x)/10;
y0 = 1;
a = 0;
b = 10;

h = 0.08;
X1 = {};
Y1 = {};
X2 = {};
Y2 = {};

for i = 1:8
    [x1, y1] = EulerE(f, a, b, y0, h);
    [x2, y2] = EulerI(f, a, b, y0, h);
    X1{i} = x1;
    Y1{i} = y1;
    X2{i} = x2;
    Y2{i} = y2;
    h = h - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
xlim([0 15]);
ylabel('y');
title('График решения на отрезке [0, 10] для разного шага (явный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure
plot(X2{1}, Y2{1}, X2{2}, Y2{2}, X2{3}, Y2{3}, X2{4}, Y2{4}, X2{5}, Y2{5}, X2{6}, Y2{6}, X2{7}, Y2{7}, X2{8}, Y2{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0, 10] для разного шага (неявный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01')
grid minor

a1 = 0.08;
b1 = 1;

h1 = 0.08;

X1 = {};
Y1 = {};
X2 = {};
Y2 = {};

for i = 1:8
    [x1, y1] = EulerE(f, a1, b1, y0, h1);
    [x2, y2] = EulerI(f, a1, b1, y0, h1);
    X1{i} = x1;
    Y1{i} = y1;
    X2{i} = x2;
    Y2{i} = y2;
    h1 = h1 - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 1] для разного шага (явный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure
plot(X2{1}, Y2{1}, X2{2}, Y2{2}, X2{3}, Y2{3}, X2{4}, Y2{4}, X2{5}, Y2{5}, X2{6}, Y2{6}, X2{7}, Y2{7}, X2{8}, Y2{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 1] для разного шага (неявный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01')
grid minor

a2 = 0;
b2 = 1;

h2 = 0.08;

Err1 = zeros(1, 8);
Err2 = zeros(1, 8);
H = zeros(1, 8);

for i = 1:8
    [x1, y1] = EulerE(f, a2, b2, y0, h2);
    [x2, y2] = EulerI(f, a2, b2, y0, h2);
    
    err1 = max(abs(ft(x1) - y1));
    err2 = max(abs(ft(x2) - y2));
    Err1(i) = err1;
    Err2(i) = err2;
    
    H(i) = h2;
    h2 = h2 - 0.01;
end

figure
semilogy(H, Err1, 'o', H, Err2, 'o')
xlabel('x');
ylabel('err');
title('График погрешности в зависимости от шага');
legend('Явный метод', 'Неявный метод', 'Location', 'NorthWest')
grid minor
