clc
clear all
close all
Q = -50.7;
f = @(x)x^4 - 7.2*x^3+9.5*x^2-7*x-2.5;
E = [];
Delta = [];
N = [];
for i = 1:15
  eps = 10^(-i);
  E = [E eps];
[q,i] = Simp(f,0,3,eps);
Delta = [Delta abs(q-Q)];
N = [N i];
end

figure
y = @(x) x;
x = 1e-15:0.1:1;
loglog(x,y(x))
hold on
loglog(E,Delta)
grid on
title('График зависимости погрешности от точности')
xlabel('\epsilon', FontSize=18)
ylabel('\delta', FontSize=18)
legend('y = x')

figure
semilogx(E, N)
grid on
title('График зависимости кол-ва итераций от точности')
xlabel('\epsilon', FontSize=18)
ylabel('N', FontSize=18)
