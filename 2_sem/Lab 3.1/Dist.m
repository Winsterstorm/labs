clc
clear all
close all
Q = 26/15;
E = 10.^(-1:-1:-4);
Delta = zeros(3,4);
N = zeros(3,4);
for j = 1:3
    f = @(x)x^4 - z(j)*7.2*x^3+9.5*x^2-7*x-2.5;
for i = 1:4
  eps = 10^(-i);
[q,k] = Simp(f,-1,1,eps);
Delta(j,i) =  abs(q-Q);
N(j,i) =  k;
end
end


figure
y = @(x) x;
x = 1e-4:0.01:0.1;
loglog(x,y(x))
grid on
hold on
loglog(E,Delta(1,:))
hold on
loglog(E,Delta(2,:))
hold on
loglog(E,Delta(3,:))
legend('y = x', '1%','2%','3%')
title({'График зависимости погрешности от', "точности с возмущением"})
xlabel('\epsilon', FontSize=18)
ylabel('\delta', FontSize=18)


figure
semilogx(E,N(1,:))
grid on
hold on
semilogx(E,N(2,:))
hold on
semilogx(E,N(3,:))
legend( '1%','2%','3%')
title({'График зависимости кол-ва итераций', "от точности с возмущением"})
xlabel('\epsilon', FontSize=18)
ylabel('N', FontSize=18)


function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end