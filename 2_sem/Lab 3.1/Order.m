clc
clear all
close all
eps = 1e-10;
Q = 26/15;
f = @(x)x^4 - 7.2*x^3+9.5*x^2-7*x-2.5;
[q,i, step, pV] = Simp(f,-1,1,eps);
Delta = abs(pV-Q);

figure 
Ls = log2(step);
LD = log2(Delta);
plot(Ls, LD)
grid on
title({'График зависимости','фактической погрешности от шага сетки'})
xlabel('h', FontSize=18)
ylabel('\delta', FontSize=18)

p = polyfit(Ls,LD,1);
n = p(1)
C = 2^(p(2))

