clc, clear all, close all;
f = @(x) x.^4 - 6.2 * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
a = 1;
b = 2;
exact = integral(f, a, b);

% f1 = @(x) x.^4 - 6.2 * z(1)  * x.^3 + 3.5 * x.^2 -7 * x - 2.1);
% f2 = @(x) x.^4 - 6.2 * z(2) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
% f3 = @(x) x.^4 - 6.2 * z(3) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;

Eps = [];
Er1 = [];
Er2 = [];
Er3 = [];
count1 = 0;
count2 = 0;
count3 = 0;
for i = -4 : -1
    eps = 10.^i;
    f1 = @(x) x.^4 - 6.2 * z(1)  * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    f2 = @(x) x.^4 - 6.2 * z(2) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    f3 = @(x) x.^4 - 6.2 * z(3) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    Eps = [Eps eps];
    [I1, count1] = Ad_Simp (f1, a, b, eps, count1);
    Er1 = [Er1 abs(exact - I1)];
    [I2, count2] = Ad_Simp (f2, a, b, eps, count2);
    Er2 = [Er2 abs(exact - I2)];
    [I3, count3] = Ad_Simp (f3, a, b, eps, count3);
    Er3 = [Er3 abs(exact - I3)];
end
plot(log2(Eps), log2(Eps))
hold on
plot(log2(Eps), log2(Er1))
hold on
plot(log2(Eps), log2(Er2))
hold on
plot(log2(Eps), log2(Er3))
grid on
xlabel('Заданная точность')
ylabel('Ошибка')
title('Зависимость ошибки от точности')
legend('Биссектрисса','1%','2%','3%')


Iter1 = [];
Iter2 = [];
Iter3 = [];
count1 = 0;
count2 = 0;
count3 = 0;
for i = -4 : -1
    eps = 10^i;
    f1 = @(x) x.^4 - 6.2 * z(1)  * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    f2 = @(x) x.^4 - 6.2 * z(2) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    f3 = @(x) x.^4 - 6.2 * z(3) * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
    [I1,count1] = Ad_Simp (f1, a, b, eps,count1);
    Iter1 = [count1 Iter1];
    [I2,count2] = Ad_Simp (f2, a, b, eps,count2);
    Iter2 = [count2 Iter2];
    [I3,count3] = Ad_Simp (f3, a, b, eps,count3);
    Iter3 = [count3 Iter3];
end
figure
plot(log2(Eps),log2(Iter1))
hold on
plot(log2(Eps),log2(Iter2))
hold on
plot(log2(Eps),log2(Iter3))
grid on
xlabel('Заданная точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности')
legend('1%','2%','3%')

function [dist]= z(alfa)
    if alfa == 0
        dist = 1;
    else
    dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
        if dist == 1
         [dist] = z(alfa);
        end
    end
end
