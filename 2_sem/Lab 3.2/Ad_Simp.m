function [I, count,F] = Ad_Simp(f, a, b, eps, count)
    D = [a, b];
    count  = count + 1;
    I1 = Simp(f, a, b, eps, 1);
    I2 = Simp(f, a, b, eps, 2);
    dev = abs(I1 - I2);
    if dev > eps

        c = (a + b) / 2;
        D = [D c]
        if a-c ~= 0 || b-c ~= 0
            
            [Ia] = Ad_Simp(f, a, c, eps , count);
            [Ib] = Ad_Simp(f, c, b, eps , count);
            I = Ia + Ib;
        end
    else
        if a ~= b
            I = I2;
        end
    end
    F = [a];
    for i = 1 : length(D)
        for j = 1 : length(F) 
            if D(i) ~= F(j)
                F = [F D(i)];
            end
        end
    end
    F = sort(F);
    
end