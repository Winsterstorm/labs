clc
clear all
close all
f = @(t, y) (3*t.^2.*exp(-t)-(t+1).*y)./t;
a = 1;
b = 5;
y0 = exp(-1);
h = 1;
tol=1e-3;
H=[];
iter=0;
ERR=[];
[t, y, H, ERR]= runge_mid(f, a, b, y0, h, tol, H, ERR);
t=[t 5];
% [t, y, err, H,iter, ERR] = midpoint_runge_kutta(f, y0, a, b, h, tol, H,iter, ERR)
% plot(t,y, 'LineWidth',2)
% hold on
% hold on
% grid on
% grid minor
% title('������� ������ ����')
% xlabel('��� x')
% ylabel('��� y')

%6.2) ������ ��������� ���� �� �������
s = (b-a)/size(H,2);
T = 1:s:5;
H=[1,H];
plot(T,log2(H),'LineWidth',2)
grid on
grid minor
title('��������� ���� �� �������')
xlabel('��� x')
ylabel('��� y')

%7)
% Tol = [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9];
% Err= [0.0029, 7.5718e-04, 1.0716e-05, 2.8988e-08, 4.0830e-07, 3.5373e-08, 9.2894e-09,6.0191e-10];
% loglog(Tol, Err, 'LineWidth',2)
% title('����������� ������ �� ��������')
% xlabel('��������')
% ylabel('������')
% grid on
% grid minor

%8)
% loglog(H, ERR, 'LineWidth',2)
% grid on
% grid minor
% title('����������� ����������� ������ �� �������� ����')
% xlabel('�������� ����')
% ylabel('������')


%9)
% Tol = [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8];
% Iter = [1, 3, 30, 60, 240, 959, 1917];
% loglog(Tol, Iter, 'LineWidth',2)
% title('���������� ����� �������� �� ��������')
% xlabel('��������')
% ylabel('����� ��������')
% grid on
% grid minor

% x=1:0.1:5;
% y1=x.^2.*exp(-x);
% plot(x,y1, 'LineWidth',2)
% grid on
% grid minor
% title('������� ������ ����')
% xlabel('��� x')
% ylabel('��� y')

