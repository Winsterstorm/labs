function [t, y3, H, ERR]= runge_mid(f, a, b, y0, h, tol, H, ERR)
t=[];
y3=[y0];
x=a;
while x < b
    eps=0;
    [t1, y1] = midpoint(f, x, x+h, y0, h, y3);
    [t2, y2] = midpoint(f, x, x+h, y0, h/2, y3);
    eps=abs(y1(end)-y2(end))/3;
    if eps > tol
        h=h/2;
    else
        x = x+h;
        t=[t t2(1, 1:size(t2,2)-1)];
        y3=[y3 y2(1, 2:size(y2,2))];
        H=[H h];
        ERR=[ERR eps];
    end
    
end
end