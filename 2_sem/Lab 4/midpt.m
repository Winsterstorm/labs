function [t,y] = midpt(f, a, b, y0, h, y3)
t = a:h:b;
y = zeros(size(t));
y(1) = y3(end);
for i = 2:length(t)
    k1 = feval(f, t(i-1), y(i-1));
    k2 = feval(f, t(i-1) + h/2, y(i-1) + h/2 * k1);
    y(i) = y(i-1) + h * k2;
end
end