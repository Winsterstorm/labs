syms x
f = (x^5 - 4.2 * x^3 + 3.5 * x^2 - 7 * x) * cos(2 * x);
a = -1;
b = 1;

I0 = int(f, a, b);
I0 = double(I0);

fun = matlabFunction(f);

Err = zeros(1, 10);
ErrR = zeros(1, 10);
Eps = zeros(1, 10);
Iter = zeros(1, 10);
N = zeros(1, 10);
H = zeros(1, 10);

for j = 1 : 7
    
    eps = 10^-j;
    
    [i, ir, iter, h] = E_Int(fun, a, b, eps);

    Err(j) = abs(i - I0);
    ErrR(j) = abs(ir - I0);
    Eps(j) = eps;
    Iter(j) = iter;
    H(j) = h;
    
end

d = diff(f, 2 * j);
d = matlabFunction(d);
x = -1 : 0.01 : 1;
y = d(x);
m = max(y);
M(j - 1) = m * (factorial(j)^4) * ((b - a)^(2 * j + 1)) / ((factorial(2 * j)^3) * (2 * j + 1));
    
figure
plot(log2(Eps), log2(Err), log2(Eps), log2(ErrR), log2([1e-1, 1e-7]), log2([1e-1, 1e-7]), '--r')
grid on
title("График зависимости ошибки от заданной точности")
xlabel("Точность (по оси отложены показатели степени 2)")
ylabel("Фактическая ошибка (по оси отложены показатели степени 2)")
legend('Ошибка для интеграла без уточнения' ,'Ошибка для уточненного интеграла')

figure
plot(log2(Eps), log2(Iter))
grid on
title("График зависимости затраченного числа итераций от заданной точности")
xlabel("Точность (по оси отложены показатели степени 2)")
ylabel("Число итераций (по оси отложены показатели степени 2)")

figure
loglog(H, Err, H, (4 * (2^3) * 2 / 216) .* H.^3, '--r', H, (33.9 * (2^3) * 2 / 216) .* H.^3, '--r')
grid on
title("График зависимости ошибки от длины отрезка разбиения")
xlabel("Длина отрезка разбиения")
ylabel("Фактическая ошибка")
legend('Фактическая ошибка', 'Линии максимальной и минимальной теоретических ошибок')

