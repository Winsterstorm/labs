function [T, C] = Rado(n)
    
    syms x

    f = (1 + x) ^ (n) * (1 - x)^(n - 1);
    P = diff(f, n - 1);
    P = P / (x + 1);
    P = simplify(P);
    
    p = sym2poly(P);
    T = roots(p);
    T = sort(T)';
    
    T = [-1, T];
    
    A = zeros(n);
    b = zeros(n, 1);
    
    for i = 1 : n
        A(i, :) = T.^(i - 1);
        
        if mod(i, 2) ~= 0
            b(i) = 2 / i;
        else
            b(i) = 0;
        end
    end

    C = A \ b;
    C = C';
    
end