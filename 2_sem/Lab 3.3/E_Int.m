function [i, ir, iter, h] = E_Int(fun, a, b, eps)
    
    [T, C] = Radau(2);
    [T1, C1] = Radau(3);
    
    A = (T + ones(1, 2)) .* (0.5 * (b - a)) + a .* ones(1, 2);
    B = C .* (0.5 * (b - a));
    
    A1 = (T1 + ones(1, 3)) .* (0.5 * (b - a)) + a .* ones(1, 3);
    B1 = C1 .* (0.5 * (b - a));
    
    i = B(1) * fun(A(1)) + B(2) * fun(A(2));
    i0 = B1(1) * fun(A1(1)) + B1(2) * fun(A1(2)) + B1(3) * fun(A1(3));
   
    iter = 5;
    
    if abs(i - i0) / 7 <= eps
        return
    end
    
    n = 2;

    while(1)
        
        I = i;
        i = 0; 
        i0 = 0;
        D = 0;
        h = (b - a) / n;
        
        for j = 1 : n
            
            a1 = a + (j - 1) * h;
            b1 = a + j * h;
            
            A = (T + ones(1, 2)) .* (0.5 * (b1 - a1)) + a1 .* ones(1, 2);
            B = C .* (0.5 * (b1 - a1));

            A1 = (T1 + ones(1, 3)) .* (0.5 * (b1 - a1)) + a1 .* ones(1, 3);
            B1 = C1 .* (0.5 * (b1 - a1));

            i = i + B(1) * fun(A(1)) + B(2) * fun(A(2));
            i0 = i0 + B1(1) * fun(A1(1)) + B1(2) * fun(A1(2)) + B1(3) * fun(A1(3));
            
            iter = iter + 5;
            d = abs(i - i0) / 7;
            D = D + d;
            
        end
        
        ir = (8 * i - I) / 7;
        
        if D <= eps
            break
        else
            n = n * 2;
        end
        
    end
    
end