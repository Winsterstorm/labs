clc
clear all
close all

a1 = 0;
b1 = 3;
a2 = 0;
b2 = 1;
a3 = 0;
b3 = 3;
f2 = @(x,y) y.^3 -7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5;
f4 = @(x,y,z) y.^3 -7.2.*x.^3 + 9.5.*x.^2 - 7.*z.^2 - 2.5;
y1 = @(x) 1/2.*sqrt(1-4/9.*(x-1.5).^2)+0.5;
y2 = @(x) -1/2.*sqrt(1-4/9.*(x-1.5).^2)+0.5;

Q2 = -98.55;
Q3 = -67.35770998837354;
Q4 = -390.15;
Delta2 = [];
Delta3 = [];
Delta4 = [];
E = [];
for p = 2:10
    eps = 10^(-p);
    E = [E eps];
    q2 = integral2(f2,a1,b1, a2, b2,'RelTol',0, 'AbsTol', eps);
    q3 = integral2(@f3,a1,b1, y2, y1,'RelTol',0, 'AbsTol', eps);
    q4 = integral3(f4,a1,b1, a2, b2,a3,b3,'RelTol',0, 'AbsTol', eps);
    Delta2 = [Delta2 abs(Q2 - q2)];
    Delta3 = [Delta3 abs(Q3 - q3)];
    Delta4 = [Delta4 abs(Q4 - q4)];
end
figure
loglog(E, Delta2)
hold on
loglog(E, Delta4)
grid on
hold on 
loglog(E, Delta3)
xlabel('\epsilon')
ylabel('\delta')
title('График зависимости погрешности от точности')
legend('P2(x,y)', 'P3(x,y)', 'P4(x,y,z)')




% function [V] = f3(x,y)
% f2 = @(x,y) y.^3 -7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5;
% a1 = 0;
% b1 = 3;
% a2 = 0;
% b2 = 1;
% s = 4.*(x-b1/2).^2/(b1-a1)^2 + 4.*(y-b2/2).^2/(b2-a2)^2;
% V = f2(x,y);
% for i = 1:size(x,1)
%     for j = 1:size(x,2)
%         if s(i,j)>=1
%             V(i,j) = 0;
%         end
%     end
% end
% end

function [V] = f3(x,y)
a1 = 0; 
b1 = 3;
 a2 = 0;
 b2 = 1;
V = zeros(size(x));
 for i = 1:size(x,1)
    for j = 1:size(x,2)
         s = 4*(x(i,j)-b1/2)^2/(b1-a1)^2 + 4*(y(i,j)-b2/2)^2/(b2-a2)^2;
        if s<1
        V(i,j) = y(i,j)^3 -7.2*x(i,j)^3 + 9.5*x(i,j)^2 - 7*x(i,j) - 2.5;
         end
     end
 end

end