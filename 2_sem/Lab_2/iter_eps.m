clc
clear all
close all
N1 = [];
N2 = [];
n = 10;


E = 10.^(-2:-1:-14);
for s = 5:-4:1
    for k = 1:13
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, s, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
     
    x0 = rand(n,1);
    [d3,V,i] =Rev(A, x0,1000, E(k));


    if s ==1
    N1= [N1 i];
    else
    N2 = [N2 i];
    end
    end
end

figure
semilogx(E,N1)
hold on
semilogx(E,N2)
grid on
title({'График зависимости кол-ва','итераций от точности'})
xlabel('\epsilon')
ylabel('N')
legend('Хорошая отд.','Плохая отд.')
