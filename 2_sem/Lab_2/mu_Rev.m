clc
clear all
close all
format long
n = 10;
mu = [];
Delta1 = [];
Delta2 = [];


for k = 1:10
   A = rand(n);
   X01 = 1/k:1/k:10/k;
   [Q,R]=qr(A);
R=R-diag(diag(R))+diag(X01);
A=Q*R*Q';

[V,L] = eig(A);
    [Vh, Lh] = eig(A);
    VV = Vh;
    L2 = sort(diag(Lh),'ascend');
    V = zeros(1,10);
     [r2, c2] = find(Lh == L2(1));
     V = Vh(:, c2);
L = sort(diag(L));

mu = [mu abs(L(2)-L(1))];

x0 = rand(n,1);
eps = 1e-15;
[L1,X] = Rev(A,x0,10000,eps);
sinus = sqrt(abs(1-((V'*X)/(norm(V)*norm(X)))^2));
Delta1 = [Delta1 abs(L1 - L(1))];
Delta2 = [Delta2 sinus];
end

figure
semilogy(mu,Delta1)
grid on
hold on
semilogy(mu,Delta2)
title({'График зависимости погрешности','от отделимости'})
ylabel('\delta')
legend('СЧ','СВ')