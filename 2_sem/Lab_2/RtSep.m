function [B, C] = RtSep(A)

P = Pl(A);

B = [];
C = [];

m1 = 0;
m2 = 11;
step = 0.01;

for x = m2:-step:m1
    
    b = x;
    c = x+step;
    
    y1 = PlVal(P, b);
    y2 = PlVal(P, c);
    
    if y1*y2 < 0
        B = [B b];
        C = [C c];
    end
    
end

end