function [L,X,iter] = Rev(A,x0,maxit,eps)
for i = 1:maxit
if i == 1
    x1 = x0;
end
x2 = Rotate(A,x1);
s1 = x1(1)/x2(1);
s2 = x1(2)/x2(2);
if abs(s1-s2) < eps
    X = x2/norm(x2);
    L = s1;
    iter = i;
    break
end
x1 = x2;
end
end