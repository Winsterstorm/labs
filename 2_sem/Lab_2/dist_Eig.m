clc
clear all
close all
N = 1e3;
Delta1 = [];
Delta2 = [];
Delta3 = [];
Delta4 = [];
n = 10;
eps = 1e-15;
for s = 5:-4:1
    for k = 0:5
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, s, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
    
    [V1, L1] = eig(A);
    m1 = max(L1(:));
    [r1, c1] = find(L1 == m1);
    
    Ah = hess(A);
    [Vh, Lh] = eig(Ah);
    L2 = sort(diag(Lh),'descend');
    V2 = zeros(10);

    for i = 1:10
        [r2, c2] = find(Lh == L2(i));
        V2(:, i) = Vh(:, c2);
    end
    V2 = abs(V2);
     M = A;
    M(1,:) = M(1,:)*z(k);
    
    [V, d3] = Eig(M, N, eps);
    d3 = sort(d3,'descend');
    V = abs(V);
    
    for i = 1:10
        errx = abs(sin(acos((V2(:, i)'*V(:, i))/(norm(V(:, i))*norm(V2(:, i))))));
        Err(i) = errx;
    end

    errx2 = max(Err);


    if s ==1
    Delta1 = [Delta1 norm((D'-d3)./D')];
    Delta2 = [Delta2 errx2];
    else
    Delta3 = [Delta3 norm((D'-d3)./D')];
    Delta4 = [Delta4 errx2];
    end
    end
end

figure
p = 0:5;
plot(p,Delta1)
grid on
hold on
plot(p,Delta2)
title({'График зависимости относительной погрешности';'от величины возмущения, хорошая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ','СВ')


figure
plot(p,Delta3)
grid on
hold on
plot(p,Delta4)
title({'График зависимости относительной погрешности';'от величины возмущения, плохая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ','СВ')

function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end