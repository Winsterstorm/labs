clc, clear all, close all

N = 1e3;
eps = 1e-14;

Mu = [];
Delta1 = [];
Delta2 = [];

    
for n = 9:-0.5:1
    
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, n, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
    
    [V1, L1] = eig(A);
    m1 = max(L1(:));
    [r1, c1] = find(L1 == m1);
    
    Ah = hess(A);
    [Vh, Lh] = eig(Ah);
    L2 = sort(diag(Lh),'descend');
    V2 = zeros(10);

    for i = 1:10
        [r2, c2] = find(Lh == L2(i));
        V2(:, i) = Vh(:, c2);
    end
    V2 = abs(V2);
    
    mu = D(1)/D(2);
    
    Mu = [Mu mu];
    
    [V, d3] = Eig(A, N, eps);
    d3 = sort(d3,'descend');
    Delta1 = [Delta1 norm(D'-d3)];
    V = abs(V);
    
    for i = 1:10
        errx = abs(sin(acos((V(:, i)'*V2(:, i))/(norm(V(:, i))*norm(V2(:, i))))));
        Err(i) = errx;
    end

    errx2 = max(Err);
    Delta2 = [Delta2 errx2];
end

figure
semilogy(Mu, Delta1)
hold on
grid on
semilogy(Mu, Delta2)
title({'График зависимости погрешности','СЧ и СВ от отделимости'})
xlabel('\mu')
ylabel('\delta')
legend('СЧ','СВ')