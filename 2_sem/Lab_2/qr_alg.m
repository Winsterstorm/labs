function [X,iter] = qr_alg(A,maxit,eps)
A = hess(A);
for i = 1:maxit
    D = diag(A);
[Q, R] = qr(A)
A1 = R*Q
D1 = diag(A1);
norm(D1-D)
if norm(D1 - D)<eps
    iter = i;
    X = sort(D1);
break
end
A = A1;
end