function [X] = Rotate(A,b)
m = size(A,1);
for k = 1:m-1
for n = k+1:m
    c = A(k,k)/(A(k,k)^2+A(n,k)^2)^(1/2);
    s = A(n,k)/(A(k,k)^2+A(n,k)^2)^(1/2);
    T = eye(m);
    T(k,k) = c;
    T(n,n) = c;
    T(k,n) = s;
    T(n,k) = -s;
    A = T*A;
    b = T*b;
end
end
X = A\b;
end
