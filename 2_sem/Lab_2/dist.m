clc
clear all
close all
Delta1 = [];
Delta2 = [];
Delta3 = [];
Delta4 = [];
n = 10;
eps = 1e-12;
for s = 5:-4:1
    for k = 0:5
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, s, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
     
    [Vh, Lh] = eig(A);
    VV = Vh;
    L2 = sort(diag(Lh),'ascend');
    V2 = zeros(1,10);
     [r2, c2] = find(Lh == L2(1));
     V2 = Vh(:, c2);
     M = A;
    M(1,:) = M(1,:)*z(k);
    x0 = rand(n,1);
    [d3,V] =Rev(M, x0,1000, eps);
    
   errx2 = abs(sin(acos((V2'*V)/(norm(V)*norm(V2)))));
    


    if s ==1
    Delta1 = [Delta1 abs((D(n)-d3)/D(n))];
    Delta2 = [Delta2 errx2];
    else
    Delta3 = [Delta3 abs((D(n)-d3)/D(n))];
    Delta4 = [Delta4 errx2];
    end
    end
end

figure
p = 0:5;
plot(p,Delta1)
grid on
hold on
plot(p,Delta2)
title({'График зависимости относительной погрешности';'от величины возмущения, хорошая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ','СВ')



figure
plot(p,Delta3)
grid on
hold on
plot(p,Delta4)
title({'График зависимости относительной погрешности';'от величины возмущения, плохая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ','СВ')

function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end