clc
clear all
close all
Delta1 = [];
Delta2 = [];
Delta3 = [];
Delta4 = [];
n = 10;


E = 10.^(-2:-1:-14);
for s = 5:-4:1
    for k = 1:13
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, s, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
     
    [Vh, Lh] = eig(A);
    L2 = sort(diag(Lh),'ascend');
    V2 = zeros(1,10);
     [r2, c2] = find(Lh == L2(1));
     V2 = Vh(:, c2);
    x0 = rand(n,1);
    [d3,V] =Rev(A, x0,1000, E(k));
    
   errx2 = abs(sin(acos((V2'*V)/(norm(V)*norm(V2)))));
    


    if s ==1
    Delta1 = [Delta1 abs((D(n)-d3)/D(n))];
    Delta2 = [Delta2 errx2];
    else
    Delta3 = [Delta3 abs((D(n)-d3)/D(n))];
    Delta4 = [Delta4 errx2];
    end
    end
end

figure
loglog(E,Delta1)
hold on
loglog(E,Delta2)
hold on
y = @(x) x;
x = 1e-15:0.1:1;
loglog(x,y(x))
grid on
title({'График зависимости погрешности','от точности, хорошая отд.'})
xlabel('\epsilon')
ylabel('\delta')
legend('СЧ','СВ')


figure
loglog(E,Delta3)
hold on
loglog(E,Delta4)
hold on
loglog(x,y(x))
grid on
title({'График зависимости погрешности','от точности, плохая отд.'})
xlabel('\epsilon')
ylabel('\delta')
legend('СЧ','СВ')