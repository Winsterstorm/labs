clear all
close all
clc
format long
n = 10;
mu = [];
Delta = [];


for k = 1:10
   A = rand(n);
   X01 = 1/k:1/k:10/k;
   [Q,R]=qr(A);
R=R-diag(diag(R))+diag(X01);
A=Q*R*Q';

[V,L] = eig(A);
L = sort(diag(L));

mu = [mu abs(L(2)-L(1))];

eps = 1e-15;
[L1] = qr_alg(A,100000,eps);
Delta = [Delta norm(L1-L)];
end

figure
semilogy(mu,Delta)
grid on
title({'График зависимости погрешности','от отделимости'})
xlabel('\mu')
ylabel('\delta')
legend('СЧ')