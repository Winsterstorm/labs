clc
clear all
close all
Delta1 = [];
Delta2 = [];
n = 10;
eps = 1e-10;
for s = 5:-4:1
    for k = 0:5
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, s, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
    

     D = sort(D,'ascend');
    [Vh, Lh] = eig(A);
    VV = Vh;
    L2 = sort(diag(Lh),'ascend');
    V2 = zeros(1,10);
     [r2, c2] = find(Lh == L2(1));
     V2 = Vh(:, c2);
     M = A;
    M(1,:) = M(1,:)*z(k);
    [X] = qr_alg(M,10000, eps);


    if s ==1
    Delta1 = [Delta1 norm((D-X')./D)];
    else
    Delta2 = [Delta2 norm((D-X')./D)];
    end
    end
end

figure
p = 0:5;
plot(p,Delta1)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения, хорошая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ')


figure
plot(p,Delta2)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения, плохая отд.'})
xlabel('\alpha')
ylabel('\delta')
legend('СЧ')

function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end