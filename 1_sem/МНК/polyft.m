clc
clear all
close all

f = @(x) 3.*x.^4+8.*x.^3+6.*x.^2-10;
x = -3:0.1:4;
y = f(x) + 40*randn(size(x));
x1 = -3:0.01:4;
subplot(6,1,1);
p1 = polyfit(x,y,1);
f1 = polyval(p1,x1);


plot(x1,f1)
hold on
plot(x,y,'.')
grid on


subplot(6,1,2);
p1 = polyfit(x,y,2);
f1 = polyval(p1,x1);


plot(x1,f1)
hold on
plot(x,y,'.')
grid on


subplot(6,1,3);
p1 = polyfit(x,y,3);
f1 = polyval(p1,x1);


plot(x1,f1)
hold on
plot(x,y,'.')
grid on

subplot(6,1,4);
p1 = polyfit(x,y,4);
f1 = polyval(p1,x1);

err = f(x1)-f1;


plot(x1,f1)
hold on
plot(x,y,'.')

 grid on

subplot(6,1,5);
p1 = polyfit(x,y,5);
f1 = polyval(p1,x1);

x1 = -3:0.01:4;
plot(x1,f1)
hold on
plot(x,y,'.')

grid on


subplot(6,1,6);
plot(x1,err)
grid on