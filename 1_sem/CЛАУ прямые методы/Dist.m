%возмущение
clc
clear all
close all

 B = rand(10);
 [M,I] = min(B);
 [N,J] = min(M); %получаем индексы наименьшего элемента матрицы
Delta1 = [];
Delta2 = [];

for n = 0:3 
 A = B;
 x = ones(10,1);
 b = A*x;
 for i = 1:length(b)
    b(i) = b(i)*z(n);
 end
 X = Rotate(A,b);
 Delta1 = [Delta1 norm(X-x,"inf")/norm(x,"inf")]; 
 
A(I(J),J) = A(I(J),J)*z(n);
 b = A*x;
 X = Rotate(A,b);
 Delta2 = [Delta2 norm(X-x,"inf")/norm(x,"inf")]; 
end

figure
p = 0:3;
plot(p,Delta1)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения в правой части'})
xlabel('\alpha')
ylabel('\delta')

figure
plot(p,Delta2)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения в элементе матрицы'})
xlabel('\alpha')
ylabel('\delta')


function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end

