clc
clear all
close all
C = [];
t = [];
M = rand(10);
for i = 0:10
A = M;
[u, d, v] = svd(A);
    d = eye(10);
d(1,1) = 10^(i/2);
    C = [C d(1,1)];
    A = u*d*v';
    x = ones(10,1);
    b = A*x;
    tic
 X = Rotate(A,b);
 t = [t toc];
end

figure 
semilogx(C,t)
title({'График зависимости времени выполнения','от числа обусловленности'})
xlabel('Cond')
ylabel('t')
grid on