clc
clear all
close all
format long


Delta = [];
C = [];
M = rand(10);
for i = 0:10
A = M;
[u, d, v] = svd(A);
    d = eye(10);
    d(1,1) = 10^(i/2);
    C = [C d(1,1)];
    A = u*d*v';
    x = ones(10,1);
    b = A*x;
 X = Rotate(A,b);
 Delta = [Delta norm(X-x,"inf")];
end

figure 
loglog(C,Delta)
title({'График зависимости точности решения','от числа обусловленности'})
xlabel('Cond')
ylabel('\delta')
grid on