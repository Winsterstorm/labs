%Метод бисекции
function [sol, i, S] = bisection(f,a, b, eps)
 S = [];
   fa = f(a);
   fb = f(b);
for i = 1:1000
    
			c = (a + b)/2;
            fc = f(c);
			if (fa*fc>0)
				a = c; 
                fa = fc;
            elseif (fa*fc<0)
				b = c;
                fb = fc;
            end
           S(i) = c;
       
			if (abs(b-a)<= eps)
                sol = c;
              i
              sol
                break
            elseif (i == 100) && (abs(b-a) > eps)
               fig = uifigure;
               uialert(fig,'Точность не достигнута','Недостаточно шагов');
            end

end
end