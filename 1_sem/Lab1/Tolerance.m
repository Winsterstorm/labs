% Переменная точность
clc
close all
DeltaEps1 = [];
Eps1 = [];
K1 = [];
DeltaEps2 = [];
Eps2 = [];
K2 = [];
DeltaEps3 = [];
Eps3 = [];
K3 = [];
DeltaEps4 = [];
Eps4 = [];
K4 = [];
DeltaEps5 = [];
Eps5 = [];
K5 = [];
DeltaEps6 = [];
Eps6 = [];
K6 = [];

for n = 1:15
    Eps1 = [Eps1 10.^(-n)];
    [sol1,i] = bisection(fun,2, 4, 10.^(-n));
    DeltaEps1 = [DeltaEps1 sol1];
    K1 = [K1 i];

     Eps2 = [Eps2 10.^(-n)];
    [sol2,i] = bisection(fun1,3, 4, 10.^(-n));
    DeltaEps2 = [DeltaEps2 sol2];
    K2 = [K2 i];

    Eps3 = [Eps3 10.^(-n)];
    [sol3,i] = bisection(fun2,-2,0.5,10.^(-n));
    DeltaEps3 = [DeltaEps3 sol3];
    K3 = [K3 i];
    
    Eps4 = [Eps4 10.^(-n)];
    [sol4,i] = Newton(fun,fun_d, 10.^(-n),4);
    DeltaEps4 = [DeltaEps4 sol4];
    K4 = [K4 i];
    
     Eps5 = [Eps5 10.^(-n)];
    [sol5,i] = Newton(fun1,fun1_d, 10.^(-n),4);
    DeltaEps5 = [DeltaEps5 sol5];
    K5 = [K5 i];
    
    Eps6 = [Eps6 10.^(-n)];
    [sol6,i] = Newton(fun2,fun2_d, 10.^(-n),-1);
    DeltaEps6 = [DeltaEps6 sol6];
    K6 = [K6 i];
end
DeltaEps1 = abs(DeltaEps1-w);
DeltaEps2 = abs(DeltaEps2-w1);
DeltaEps3 = abs(DeltaEps3-w2);
DeltaEps4 = abs(DeltaEps4-w);
DeltaEps5 = abs(DeltaEps5-w1);
DeltaEps6 = abs(DeltaEps6-w2);

figure
loglog(Eps1, DeltaEps1)
 hold on 
 loglog(Eps2, DeltaEps2)
hold on
loglog(Eps3, DeltaEps3)
hold on
 x = 1e-15:0.000001:1;
 y = @(x) x;
 loglog(x,y(x))
 
 
 title({'График 3(a) зависимости погрешности от точности';...
     'для метода бисекции'});
 xlabel('\epsilon', 'FontSize',16)
 ylabel('\delta', 'FontSize',16)
 legend('для f = 3x-4log(x)-5', ...
     'для f = x^4 - x^3 - 7x^2 - 8x - 6','-x +1/(x-1)+1/5*sin(x)','y = x')
  grid on
figure
loglog(Eps4, DeltaEps4)
 hold on 
loglog(Eps5, DeltaEps5)
 hold on
 loglog(Eps6, DeltaEps6)
 hold on
x = 1e-15:0.000001:1;
 y = @(x) x;
 loglog(x,y(x))
 
 title({'График 3(б) зависимости погрешности от точности'; ...
     'для метода Ньютона'})
 xlabel('\epsilon', 'FontSize',16)
 ylabel('\delta', 'FontSize',16)
 legend('для f = 3x-4log(x)-5', ...
     'для f = x^4 - x^3 - 7x^2 - 8x - 6','-x +1/(x-1)+1/5*sin(x)','y = x')
grid minor
figure
semilogx(Eps1,K1)
hold on
semilogx(Eps2,K2)
hold on
semilogx(Eps3,K3)
title({'График 4(а) зависимости числа шагов от точности'; ...
    'для метода бисекции'})
 xlabel('\epsilon', 'FontSize',16)
 ylabel('K', 'FontSize',16)
 legend('для f = 3x-4log(x)-5', ...
     'для f = x^4 - x^3 - 7x^2 - 8x - 6','-x +1/(x-1)+1/5*sin(x)')
 grid minor
 
 figure
semilogx(Eps4,K4)
hold on
semilogx(Eps5,K5)
hold on
semilogx(Eps6,K6)

title({'График 4(б) зависимости числа шагов от точности'; ...
    'для метода Ньютона'})
 xlabel('\epsilon', 'FontSize',16)
 ylabel('K', 'FontSize',16)
 legend('для f = 3x-4log(x)-5', ...
     'для f = x^4 - x^3 - 7x^2 - 8x - 6','-x +1/(x-1)+1/5*sin(x)')
 grid minor
 