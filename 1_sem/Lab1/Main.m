%Графики функций
clc
close all
clear all
format long
x_zero = [2 4];
x1_zero = [3 4];
x2_zero = [-2 0.5];
x = 2.0:0.1:4.0;
x1 = -2.0:0.1:4.0;
x2 = -2:0.01:0.8;


fun = @(x) 3*x-4*log(x)-5;
fun1 = @(x) x.^4 - x.^3 - 7*x.^2 - 8*x - 6;
fun_d= @(x) 3 - 4/x;
fun1_d = @(x) 4*x.^3 - 3*x.^2 - 14*x - 8;
 fun2 = @(x) (x-1).^(-1)+1/5*sin(x)-x;
 fun2_d = @(x) 1/5*cos(x)-(x-1).^(-2)-1;




figure
plot(x, fun(x));
title('График 1 функции 1');
xlabel('X')
ylabel('Y')
grid minor
legend('3*x-4*log(x)-5')


figure
plot(x1, fun1(x1));
title('График 1 функции 2');
xlabel('X')
ylabel('Y')
grid minor
legend('x^4 - x^3 - 7*x^2 - 8*x - 6')
 
 
figure
plot(x2,fun2(x2))
title('График 1 функции 3');
xlabel('X')
ylabel('Y')
grid minor
legend('-x +1/(x-1)+1/5*sin(x)')

%Проверка корней

options = optimset('Display','iter');
 w = fzero(fun, x_zero, options)
 w1 = fzero(fun1, x1_zero, options)
w2 = fzero(fun2, x2_zero, options)
