 % Возмущение
close all
clc
clear all
format long
DDelta1 = [];
DDelta2 = [];
DDelta3 = [];
DDelta4 = [];
DDelta5 = [];
DDelta6 = [];


 for j = 0:5
     f = @(x) z(j)*3*x-4*log(x)-5;
     f1 = @(x) x.^4 - z(j)*x.^3 - 7*x.^2 - 8*x - 6;
     f2 = @(x) 1/5*sin(x)+(x-1).^(-1)-z(j)*x;
     f_d= @(x) z(j)*3 - 4/x;
     f1_d = @(x) 4*x.^3 - z(j)*3*x.^2 - 14*x - 8;
     f2_d = @(x) 1/5*cos(x)-(x-1).^(-2)-z(j);


     [sol1] = bisection(f,2,4,1e-15);
     [sol2] = bisection(f1,2,4,1e-15);
     [sol3] = bisection(f2,-2,0.5,1e-15);
     [sol4] = Newton(f,f_d,1e-15,4);
     [sol5] = Newton(f1,f1_d,1e-15,4);
     [sol6] = Newton(f2,f2_d,1e-15,-1);
     DDelta1 = [DDelta1 sol1];
      DDelta2 = [DDelta2 sol2];
       DDelta3 = [DDelta3 sol3];
        DDelta4 = [DDelta4 sol4];
         DDelta5 = [DDelta5 sol5];
        DDelta6 = [DDelta6 sol6];
     
     if j == 0
         sl1 = sol1;
         sl2 = sol2;
         sl3 = sol3;
         sl4 = sol4;
         sl5 = sol5;
         sl6 = sol6;
     end
 
 end

 
 figure
 DDelta1_str = abs(DDelta1-sl1)/sl1; %относительная погрешность
 alfa = 0:5;
 plot(alfa,DDelta1_str)
 title({'График 5(а) зависимости относительной погрешности'; ...
     'от максимальной величины возмущения';'для функции 1'})
xlabel('\alpha')
ylabel('\delta\prime')

grid on
hold on
 DDelta4_str = abs(DDelta4-sl4)/sl4; %относительная погрешность
 plot(alfa,DDelta4_str)
legend('метод бисекции','метод Ньютона')

figure
 DDelta2_str = abs(DDelta2-sl2)/sl2; %относительная погрешность
 plot(alfa,DDelta2_str)
 title({'График 5(б) зависимости относительной погрешности'; ...
     'от максимальной величины возмущения';'для функции 2'})
xlabel('\alpha')
ylabel('\delta\prime')

grid on
hold on
 DDelta5_str = abs(DDelta5-sl5)/sl5; %относительная погрешность
 plot(alfa,DDelta5_str)
 legend('метод бисекции','метод Ньютона')

figure
 DDelta3_str = abs((DDelta3-sl3)/sl3); %относительная погрешность
 plot(alfa,DDelta3_str)
 title({'График 5(в) зависимости относительной погрешности'; ...
     'от максимальной величины возмущения';'для функции 3'})
xlabel('\alpha')
ylabel('\delta\prime')

grid on
hold on
 DDelta6_str = abs((DDelta6-sl6)/sl6); %относительная погрешность
 plot(alfa,DDelta6_str)
legend('метод бисекции','метод Ньютона')


% Множитель, вносящий погрешность в коэффициент
function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end

