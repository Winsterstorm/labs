%Метод Ньютона
function [root, step, R] = Newton(f,f_d, eps, first)
root = first;
R = [];
step = 0;
if (f(root+eps)*f(root-eps))>0
    fr = f(root);
    fr_d = f_d(root);
while abs(fr/fr_d) > eps && step < 1000
    root = root - fr/fr_d; 
    step = step + 1;
    R(step) = root;
    fr = f(root);
    fr_d = f_d(root);
end
end
step
root
end
