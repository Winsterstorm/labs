%Итерации
clc
close all
[sol,i,S] = bisection(fun,2, 4, 1e-15);
Delta = abs(S-w);
figure
semilogy(Delta)
title({'График 2(а) зависимости погрешности от числа итераций'; ...
    'для функции 1, Метод бисекции'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor

[sol,i,S] = bisection(fun1,3, 4, 1e-15);
Delta = abs(S-w1);
figure 
semilogy(Delta)
title({'График 2(б) зависимости погрешности от числа итераций'; ...
    'для функции 2, Метод бисекции'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor

[sol,i,S] = bisection(fun2,-2, 0.5, 1e-15);
Delta = abs(S-w2);
figure 
semilogy(Delta)
title({'График 2(в) зависимости погрешности от числа итераций'; ...
    'для функции 3, Метод бисекции'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor

[root, step, R] = Newton(fun,fun_d, 1e-15,4);
D = abs(R-w);
figure 
semilogy(D)
title({'График 2(г) зависимости погрешности от числа итераций'; ...
    'для функции 1, Метод Ньютона'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor

[root, step, R] = Newton(fun1,fun1_d, 1e-15, 4);
D = abs(R-w1);
figure 
semilogy(D)
title({'График 2(д) зависимости погрешности от числа итераций'; ...
    'для функции 2, Метод Ньютона'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor

[root, step, R] = Newton(fun2,fun2_d, 1e-15, -1);
D = abs(R-w2);
figure 
semilogy(D)
title({'График 2(е) зависимости погрешности от числа итераций'; ...
    'для функции 3, Метод Ньютона'})
xlabel('i')
ylabel('\delta','FontSize',16)
grid minor