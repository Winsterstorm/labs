clc, close all, clear all
A = [1 2 3; 3 5 7; 1 3 4]; %точное решение - -4 -13 11
B = [3; 0; 1];
[u, d, v] = svd(A);
    d = eye(3); 
    A = u*d*v'; 
opts.UT = true;
X1 = linsolve(A, B); %точное решение
X2 = linsolve(A, B, opts); %решение для верхнетреугольной матрицы
opts.UT = false;
opts.SYM = true;
X3 = linsolve(A, B, opts); %решение для симметричной матрицы


% tol=1e-7;
% maxit = 10;
A = rand(5);
tol = 1e-7;
maxit = 100;
B = rand(5,1);
X = pcg(A, B,tol,maxit);
n = norm(B-A*X)/norm(B);
cc = cond(A);

format long

c = [];
iter = [];
for n = 1:10
    A = hilb(n);
    c(n) = cond(A);
    B = ones(n, 1);
    R = linsolve(A, B);
    tol = 1e-5;
    [X, flag,relres, iter(n)] = pcg(A, B, tol); %flag = 0, то значит все хорошо, если flag<0, то relres<tol
 
end
semilogx(c, iter, "o-")
title("Зависимость количества итераций от cond", "FontSize", 18)
xlabel("Число обусловленности матрицы", "FontSize", 14)
ylabel("Количество итераций", "FontSize", 14)
grid on
