clc
clear all 
close all
format short
n = 5;
A = hilb(n);
xt = ones(n,1); %точное решение 
b = A*xt; %правая часть СЛАУ, соответствующая xt
x1 = A\b
r1=b-A*x1 %невязка
dx1 = x1-xt %отклонение 
c = cond(A)

n = 10;
A = hilb(n);
xt = ones(n,1); %точное решение 
b = A*xt; %правая часть СЛАУ, соответствующая xt
x2 = A\b
r2=b-A*x2 %невязка
dx2 = x2-xt %отклонение 
c = cond(A)

n = 15;
A = hilb(n);
xt = ones(n,1); %точное решение 
b = A*xt; %правая часть СЛАУ, соответствующая xt
x3 = A\b
r3=b-A*x3 %невязка
dx3 = x3-xt %отклонение 
c = cond(A)