clc
clear all
close all

x = ones(10,1);
A = rand(10)+10*eye(10);
b = A*x;
N = [];
E = [];
for i = 1:15
eps = 10^(-i);
E = [E eps];
[X,k] = Jacobi(A,b,eps);
N = [N k];
end

figure 
semilogx(E,N)
grid on
xlabel('\epsilon', 'FontSize',18)
ylabel('N', 'FontSize',18)
title({'График зависимости кол-ва';'итераций от точности'})