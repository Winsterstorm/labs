function [X,k,Cur] = Jacobi(A,b,eps)
n = size(A,1);
B = zeros(n);
for i = 1:n
    for j = 1:n
B(i,i) = 0;
if i~=j
B(i,j) = -A(i,j)/A(i,i);
end
    end
end
Cur = [];


q = norm(B,"inf");
x0 = rand(size(b));
x1 = zeros(size(b));
for k = 1:1000
    for i = 1:n
        s=0;
        for j = 1:n
            if j~=i
             s = s+A(i,j)*x0(j);
            end
        end
x1(i) = 1/A(i,i)*(b(i)-s);
    end
Cur = [Cur x1];
if norm(x1-x0,"inf")<(1-q)/q*eps
    break
end
x0 = x1;
end
X = x1;
end