clc
clear all
close all
x = ones(10,1);
A = rand(10)+10*eye(10);
b = A*x;
Delta= [];
E = [];
for i = 1:15
eps = 10^(-i);
E = [E eps];
[X] = Jacobi(A,b,eps);
Delta = [Delta norm(X-x,"inf")];
end

figure 
loglog(E,Delta)
grid on
xlabel('\epsilon', 'FontSize',18)
ylabel('\delta', 'FontSize',18)
title({'График зависимости погрешности';'от точности'})