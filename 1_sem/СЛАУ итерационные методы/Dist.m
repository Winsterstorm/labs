clc
clear all
close all

n = 10;
 B = rand(n)+10*eye(n);
 x = ones(n,1);
 [L,I] = min(B);
 [N,J] = min(L);
Delta1 = [];
Delta2 = [];

for k = 0:3 
 A = B;
 b = A*x;
 for i = 1:length(b)
    b(i) = b(i)*z(k);
 end
 [X] = Jacobi(A,b,1e-14);
 Delta1 = [Delta1 norm(X-x,"inf")/norm(x,"inf")]; 
 
A(I(J),J) = A(I(J),J)*z(k);
 b = A*x;
 [X] = Jacobi(A,b,1e-14);
 Delta2 = [Delta2 norm(X-x,"inf")/norm(x,"inf")]; 
end

figure
p = 0:3;
plot(p,Delta1)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения в правой части'})
xlabel('\alpha')
ylabel('\delta, в долях')

figure
plot(p,Delta2)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения в элементе матрицы'})
xlabel('\alpha')
ylabel('\delta, в долях')


function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end

