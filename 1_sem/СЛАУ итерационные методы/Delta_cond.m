clear all
clc
close all

Delta = [];
C = [];
n = 10;
A = rand(n);
x  = ones(10,1);


for k = 1:5
    M = A;
    MD=M;
for i=1:n
    MD(i,i)=MD(i,i)+i^(k)*sum(M(i,[1:i-1 i+1:n]));
end
 C = [C cond(MD)];
 b = MD*x;
 [X] = Jacobi(MD,b,1e-15);
 Delta = [Delta norm(X-x,"inf")];
end

figure 
semilogx(C,Delta)
title({'График зависимости точности решения','от числа обусловленности'})
xlabel('Cond')
ylabel('\delta')
grid on