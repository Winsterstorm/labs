clc
clear all
close all

x = ones(10,1);
A = rand(10)+10*eye(10);
b = A*x;
Delta = [];

[X,k,C] = Jacobi(A,b,1e-15);
DD = C - ones(size(C));

for i = 1:size(C,2)
Delta = [Delta norm(DD(:,i),"inf")];
end

figure 
n = 1:k;
semilogy(n,Delta)
grid on
xlabel('i', 'FontSize',18)
ylabel('\delta', 'FontSize',18)
title({'График изменения погрешности';'с ходом итераций'})