clc
clear all
close all
C = [];
t = [];
n = 10;
A = rand(n);
x = ones(n,1);
for k = 1:6
M = A;
MD=M;
for i=1:n
    MD(i,i)=MD(i,i)+i^(k)*sum(M(i,[1:i-1 i+1:n]));
end
 C = [C cond(MD)];
 b = MD*x;
 tic
 [X] = Jacobi(MD,b,1e-15);
 t = [t toc];
end

figure 
semilogx(C,t)
title({'График зависимости времени выполнения','от числа обусловленности'})
xlabel('Cond')
ylabel('t')
grid on