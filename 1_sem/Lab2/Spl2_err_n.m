clc, clear all, close all
%ошибка интерполяции от степени полинома
%функция 2 

f = @(x) log(x);
x = 1.5:0.01:5.5;
b = 400;
c = linspace(1.5,5.5,b); %составляем множество точек

for n = 5:250
    ee = [];
    X = linspace(1.5,5.5,n); %узлы, границы и кол-во точек (кол-во отрезков, на 1 меньше)
    Y = log(X); %функция
    TT = Ermit(X, Y);
    for k = 1:b
        for j = 1: length(X)-1
            if c(k) <= X(j+1) && c(k) >= X(j)
                E = TT(j,1).*(c(k)-X(j)).^3 + TT(j,2).*(c(k)-X(j)).^2 + TT(j,3).*(c(k)-X(j)) + TT(j,4);
            end
        
        end

        %E = TT(j,1).*(c-X(j)).^3 + TT(j,2).*(c-X(j)).^2 + TT(j,3).*(c-X(j)) + TT(j,4);
        %E = TT(j,1).*(x1-X(j)).^3 + TT(j,2).*(x1-X(j)).^2 + TT(j,3).*(x1-X(j)) + TT(j,4);
        e = abs(E - f(c(k)));
        ee = [ee e];

    end

    d(n) = max(ee);

end

semilogy(1:250, d, 'b-')
title("Зависимость поточечной ошибки Эрмитова сплайна от степени полинома для функции 1")
xlabel("Степень полинома n")
ylabel("Ошибка уклонения (пролагрифмированная")
hold on
grid on



