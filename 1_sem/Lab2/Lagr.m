% Полином в форме Лагранжа
function [y] = Lagr(X, Y,x)
    function [l] = basic(x,i)
        divider = 1;
        result = 1;
        for j = 1:length(X)
            if (j~=i)
                result = result.*(x-X(j));
                divider = divider.*(X(i)-X(j));
            end
        end
        l = result/divider;
    end
y=0;
for k = 1:length(X)
    y = y+basic(x,k)*Y(k);
end
        end