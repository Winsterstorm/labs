function xp = Thicken(xx, x0, n)
%функция выводит сетку со сгущением вокруг точки х0
xp = [];
a=xx(1);
b = xx(end);
c = (a+b)/2;

for i = 1:n
    xp(1) = a;
    xp(n) = b;
    if xx(i) <= c && xx(i) >= (2*x0 - c)
        %for j = 1:(i-1)
            xp(i) = (xx(i)+xx(i+1))/2;
    
        %end
        %xp(i) = (x0+xx(i)/2); 
    end

    if xx(i) < b && xx(i) > c
        xp(i) = xx(i);
    end

end



