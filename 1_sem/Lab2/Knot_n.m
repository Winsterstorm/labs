% Ошибка/количество узлов
clc
clear all
close all

f1 = @(x) log(x);
f2 = @(x) 3.*sign(x).*x.^4+8.*x.^3+6.*x.^2-10;
x1 = 1.5:0.01:2.5;
x2 = 0.5:0.01:4.5;
x11 = 1.5:2.5;
x21 = 0.5:0.5:4.5;
Norma11 = [];
Norma12 = [];
Norma21 = [];
Norma22 = [];


for i = 5:100
    step1 = (x11(length(x11))-x11(1))/(i-1);
    step2 = (x21(length(x21))-x21(1))/(i-1);
   x11 = 1.5:step1:2.5;
   x21 = 0.5:step2:4.5;
   y11 = [];
   x12 = [];
   y12=[];
    y21 = [];
   x22 = [];
   y22=[];
   for j = 1:length(x11)
       x12 = [x12 (x11(length(x11))+x11(1))/2+(x11(length(x11))-x11(1))/2*cos((2*j)*pi/(2*length(x11)))];
       y11 = [y11 f1(x11(j))];
       y12 = [y12 f1(x12(j))];
   end
   for n = 1:length(x21)
    y21 = [y21 f2(x21(n))];
    x22 = [x22 (x21(length(x21))+x21(1))/2+(x21(length(x21))-x21(1))/2*cos((2*n)*pi/(2*length(x21)))];
     y22 = [y22 f2(x22(n))];
    end
   V11 = [];
   V12 = [];
   V21 = [];
   V22 = [];
   for k = 1:length(x1)
       V11 = [V11 f1(x1(k))-Lagr(x11,y11,x1(k))];
        V12 = [V12 f1(x1(k))-Lagr(x12,y12,x1(k))];
   end
   for h = 1:length(x2)
       V21 = [V21 f2(x2(h))-Lagr(x21,y21,x2(h))];
        V22 = [V22 f2(x2(h))-Lagr(x22,y22,x2(h))];
   end
   Norma11 = [Norma11 norm(V11)];
    Norma12 = [Norma12 norm(V12)];
    Norma21 = [Norma21 norm(V21)];
    Norma22 = [Norma22 norm(V22)];
end

figure
ii = 5:100;
semilogy(ii,Norma11)
hold on
semilogy(ii, Norma12)
title({'График зависимости погрешности от кол-ва узлов';'для полинома, функция 1'})
xlabel('n')
ylabel('\delta\prime\prime')
legend('Равномерная сетка','Чебышевская сетка')
grid on


figure
semilogy(ii, Norma21)
hold on
semilogy(ii,Norma22)
title({'График зависимости погрешности от кол-ва узлов';'для полинома, функция 2'})
xlabel('n')
ylabel('\delta\prime\prime')
legend('Равномерная сетка','Чебышевская сетка')
grid on
