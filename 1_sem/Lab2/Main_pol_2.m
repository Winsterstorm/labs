clc
clear all
close all


f2 = @(x) 3.*sign(x).*x.^4+8.*x.^3+6.*x.^2-10;

x2 = -2:0.01:2;
x216 = -2:0.8:2;
y216 = [];
x226 = [];
y226 = [];
x219 = -2:0.5:2;
y219 = [];
x229 = [];
y229 = [];
x211 = -2:0.4:2;
y211 = [];
x221 = [];
y221 = [];
for i = 1:length(x216)
    y216 = [y216 f2(x216(i))];
    x226 = [x226 (x216(length(x216))+x216(1))/2+(x216(length(x216))-x216(1))/2*cos((2*i)*pi/(2*length(x216)))];
     y226 = [y226 f2(x226(i))];
end
for i = 1:length(x219)
    y219 = [y219 f2(x219(i))];
    x229 = [x229 (x219(length(x219))+x219(1))/2+(x219(length(x219))-x219(1))/2*cos((2*i)*pi/(2*length(x219)))];
     y229 = [y229 f2(x229(i))];
end
for i = 1:length(x211)
    y211 = [y211 f2(x211(i))];
    x221 = [x221 (x211(length(x211))+x211(1))/2+(x211(length(x211))-x211(1))/2*cos((2*i)*pi/(2*length(x211)))];
     y221 = [y221 f2(x221(i))];
end

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x216,y216,x2))
hold on 
plot(x216, f2(x216),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'равномерная сетка,6 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x219,y219,x2))
hold on 
plot(x219, f2(x219),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'равномерная сетка,9 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x211,y211,x2))
hold on 
plot(x211, f2(x211),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'равномерная сетка,11 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x226,y226,x2))
hold on 
plot(x226, f2(x226),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'Чебышевская сетка,6 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x229,y229,x2))
hold on 
plot(x229, f2(x229),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'Чебышевская сетка,9 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

figure 
plot(x2,f2(x2))
grid on
hold on
plot(x2, Lagr(x221,y221,x2))
hold on 
plot(x221, f2(x221),'o','Color','green')

title({'График функции 2 и интерполирующего полинома'; ...
    'Чебышевская сетка,11 узлов'})
xlabel('x')
ylabel('y')
legend('f = 3sign(x)x^4+8x^3+6x^2-10', 'полином Лагранжа')

Delta26 = [];
Delta226 = [];
Delta29 = [];
Delta229 = [];
Delta21 = [];
Delta221 = [];
for k = 1:length(x2)
    Delta26 = [Delta26 abs(f2(x2(k))-Lagr(x216,y216,x2(k)))];
     Delta226 = [Delta226 abs(f2(x2(k))-Lagr(x226,y226,x2(k)))];
      Delta29 = [Delta29 abs(f2(x2(k))-Lagr(x219,y219,x2(k)))];
     Delta229 = [Delta229 abs(f2(x2(k))-Lagr(x229,y229,x2(k)))];
      Delta21 = [Delta21 abs(f2(x2(k))-Lagr(x211,y211,x2(k)))];
     Delta221 = [Delta221 abs(f2(x2(k))-Lagr(x221,y221,x2(k)))];
end

figure
plot(x2,Delta26);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 6 узлов'})

figure
plot(x2,Delta29);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 9 узлов'})

figure
plot(x2,Delta21);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 11 узлов'})

figure
semilogy(x2,Delta226);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Чебышевская сетка, 6 узлов'})
figure
semilogy(x2,Delta229);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Чебышевская сетка, 9 узлов'})

figure
semilogy(x2,Delta221);
grid minor
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Чебышевская сетка, 11 узлов'})

