clc
clear all
close all

f1 = @(x) log(x);
f2 = @(x) 3.*sign(x).*x.^4+8.*x.^3+6.*x.^2-10;
x1 = 1.5:0.001:2.5;
x2 = 0.5:0.001:4.5;
x11 = 1.5:2.5;
x21 = 0.5:0.5:4.5;

 Delta1 = [];
 Delta2 = [];
 Delta3 = [];
 Delta4 = [];
 Delta5 = [];
 Delta6 = [];
 Delta7 = [];
 Delta8 = [];
for i = 5:100
    
step1 = (x11(length(x11))-x11(1))/(i-1);
    step2 = (x21(length(x21))-x21(1))/(i-1);
   x11 = 1.5:step1:2.5;
   x21 = 0.5:step2:4.5;
   y11 = [];
   x12 = [];
   y12=[];
    y21 = [];
   x22 = [];
   y22=[];
   for j = 1:length(x11)
       x12 = [x12 (x11(length(x11))+x11(1))/2+(x11(length(x11))-x11(1))/2*cos((2*j)*pi/(2*length(x11)))];
       y11 = [y11 f1(x11(j))];
       y12 = [y12 f1(x12(j))];
   end
   for n = 1:length(x21)
    y21 = [y21 f2(x21(n))];
    x22 = [x22 (x21(length(x21))+x21(1))/2+(x21(length(x21))-x21(1))/2*cos((2*n)*pi/(2*length(x21)))];
     y22 = [y22 f2(x22(n))];
   end
  
   x01 = (x11(1)+x11(2))/2; %Первая точка
   x02 = x11(1)+(x11(2)-x11(1))/5; %Вторая точка
   
   x03 = (x12(4)+x12(3))/2; %Первая точка
   x04 = x12(3)+(x12(4)-x12(3))/5; %Вторая точка
   
   Delta1 = [Delta1 abs(f1(x01)-Lagr(x11,y11,x01))];
   Delta2 = [Delta2 abs(f1(x02)-Lagr(x11,y11,x02))];
   Delta3 = [Delta3 abs(f1(x03)-Lagr(x12,y12,x03))];
   Delta4 = [Delta4 abs(f1(x04)-Lagr(x12,y12,x04))];
   Delta5 = [Delta5 abs(f2(x01)-Lagr(x21,y21,x01))];
   Delta6 = [Delta6 abs(f2(x02)-Lagr(x21,y21,x02))];
   Delta7 = [Delta7 abs(f2(x03)-Lagr(x22,y22,x03))];
   Delta8 = [Delta8 abs(f2(x04)-Lagr(x22,y22,x04))];
   
   
   
end

figure
ii = 5:100;
semilogy(ii,Delta1)
 hold on
 semilogy(ii,Delta2)
title({'График зависимости погрешности в точке от кол-ва узлов';'Для функции 1,Равномерная сетка'})
xlabel('n')
ylabel('\delta')
grid on


figure
semilogy(ii,Delta3)
  hold on
  semilogy(ii,Delta4)
title({'График зависимости погрешности в точке от кол-ва узлов';'Для функции 1,Чебышевская сетка'})
xlabel('n')
ylabel('\delta')
legend('Чебышевская сетка')
grid on

figure
semilogy(ii,Delta5)
 hold on
 semilogy(ii,Delta6)
title({'График 4 зависимости погрешности в точке от кол-ва узлов';'Для функции 2,Равномерная сетка'})
xlabel('n')
ylabel('\delta')

grid on


figure
semilogy(ii,Delta7)
  hold on
  semilogy(ii,Delta8)
title({'График 4 зависимости погрешности в точке от кол-ва узлов';'Для функции 2,Чебышевская сетка'})
xlabel('n')
ylabel('\delta')

grid on

