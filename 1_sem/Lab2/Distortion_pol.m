 clc
clear all
close all

f1 = @(x) log(x);
f2 = @(x) 3.*sign(x).*x.^4+8.*x.^3+6.*x.^2-10;
x1 = 1.5:0.01:8.5;
x11 = 1.5:8.5;
x2 = -2:0.01:2;
x21 = -2:0.5:2;
FinalD11 = [];
FinalD12 = [];
FinalD21 = [];
FinalD22 = [];


for n = 0:5
y11 = [];
x12 = [];
y12 =[];
y21 = [];
x22 = [];
y22 =[];


for j = 1:length(x11)
   x12 = [x12 (x11(length(x11))+x11(1))/2+(x11(length(x11))-x11(1))/2*cos((2*j)*pi/(2*length(x11)))];
    y12 = [y12 z(n)*f1(x12(j))];
    y11 = [y11 z(n)*f1(x11(j))];
    
end
for j = 1:length(x21)
   x22 = [x22 (x21(length(x21))+x21(1))/2+(x21(length(x21))-x21(1))/2*cos((2*j)*pi/(2*length(x21)))];
    y22 = [y22 z(n)*f2(x22(j))];
    y21 = [y21 z(n)*f2(x21(j))];
    
end
D_str11 = [];
D_str12 = [];
D_str21 = [];
D_str22 = [];
for k = 1:length(x1)
D_str11 = [D_str11 abs((f1(x1(k))-Lagr(x11,y11,x1(k)))/f1(x1(k)))];
D_str12 = [D_str12 abs((f1(x1(k))-Lagr(x12,y12,x1(k)))/f1(x1(k)))];
end
for k =1:length(x2)
D_str21 = [D_str21 abs((f2(x2(k))-Lagr(x21,y21,x2(k)))/f2(x2(k)))];
D_str22 = [D_str22 abs((f2(x2(k))-Lagr(x22,y22,x2(k)))/f2(x2(k)))];
end
FinalD11 = [FinalD11 max(D_str11)];
FinalD12 = [FinalD12 max(D_str12)];
FinalD21 = [FinalD21 max(D_str21)];
FinalD22 = [FinalD22 max(D_str22)];
end

alfa = 0:5;
figure 
plot(alfa, FinalD11)
grid on 
xlabel('\alpha')
ylabel('\delta, в долях')
title({'График зависимости относительной погрешности';'от возмущения, р/м, 1'})

figure 
plot(alfa, FinalD12)
grid on 

xlabel('\alpha')
ylabel('\delta, в долях')
title({'График зависимости относительной погрешности';'от возмущения, Чебышевская, 1'})

figure 
plot(alfa, FinalD21)
grid on 
xlabel('\alpha')
ylabel('\delta, в долях')
title({'График зависимости относительной погрешности';'от возмущения, р/м, 2'})

figure 
plot(alfa, FinalD22)
grid on 
xlabel('\alpha')
ylabel('\delta, в долях')
title({'График зависимости относительной погрешности';'от возмущения, Чебышевская, 2'})


function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end
