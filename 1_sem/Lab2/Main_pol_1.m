clc
clear all
close all
d = @(x) (9.*x.^9).^(-1);

f1 = @(x) log(x);



x1 = 1.5:0.01:8.5;
x115 = 1.5:1.75:8.5;
y115 = [];
x125 = [];
y125 =[];
x118 = 1.5:8.5;
y118 = [];
x128 = [];
y128 =[];
x1110 = 1.5:0.7:8.5;
y1110 = [];
x1210 = [];
y1210 =[];


for j = 1:length(x115)
   x125 = [x125 (x115(length(x115))+x115(1))/2+(x115(length(x115))-x115(1))/2*cos((2*j)*pi/(2*length(x115)))];
    y125 = [y125 f1(x125(j))];
y115 = [y115 f1(x115(j))];
end
for j = 1:length(x118)
   x128 = [x128 (x118(length(x118))+x118(1))/2+(x118(length(x118))-x118(1))/2*cos((2*j)*pi/(2*length(x118)))];
    y128 = [y128 f1(x128(j))];
y118 = [y118 f1(x118(j))];
end
for j = 1:length(x1110)
   x1210 = [x1210 (x1110(length(x1110))+x1110(1))/2+(x1110(length(x1110))-x1110(1))/2*cos((2*j)*pi/(2*length(x1110)))];
    y1210 = [y1210 f1(x1210(j))];
y1110 = [y1110 f1(x1110(j))];
end

figure 
plot(x1,f1(x1))
grid on
hold on
plot(x1, Lagr(x115,y115,x1))
hold on 
plot(x115, f1(x115),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'равномерная сетка,5 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')

figure 
plot(x1,f1(x1))
grid on
hold on
plot(x1, Lagr(x118,y118,x1))
hold on 
plot(x118, f1(x118),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'равномерная сетка,8 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')

figure 
plot(x1,f1(x1))
grid on
hold on
plot(x1, Lagr(x1110,y1110,x1))
hold on 
plot(x1110, f1(x1110),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'равномерная сетка,10 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')



figure
plot(x1,f1(x1))
hold on
plot(x1,Lagr(x125,y125,x1))
hold on
plot(x125, f1(x125),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'Чебышевская сетка, 5 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')
grid on

figure
plot(x1,f1(x1))
hold on
plot(x1,Lagr(x128,y128,x1))
hold on
plot(x128, f1(x128),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'Чебышевская сетка, 8 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')
grid on

figure
plot(x1,f1(x1))
hold on
plot(x1,Lagr(x1210,y1210,x1))
hold on
plot(x1210, f1(x1210),'o','Color','green')
title({'График функции 1 и интерполирующего полинома'; ...
    'Чебышевская сетка, 10 узлов'})
xlabel('x')
ylabel('y')
legend('f = ln(x)', 'полином Лагранжа')
grid on
Delta15 = [];
Delta115= [];
Delta18 = [];
Delta118= [];
Delta110 = [];
Delta1110= [];
for n = 1:length(x1)
    Delta15 = [Delta15 abs(f1(x1(n))-Lagr(x115,y115,x1(n)))];
    Delta115 = [Delta115 abs(f1(x1(n))-Lagr(x125,y125,x1(n)))];
     Delta18 = [Delta18 abs(f1(x1(n))-Lagr(x118,y118,x1(n)))];
    Delta118 = [Delta118 abs(f1(x1(n))-Lagr(x128,y128,x1(n)))];
     Delta110 = [Delta110 abs(f1(x1(n))-Lagr(x1110,y1110,x1(n)))];
    Delta1110 = [Delta1110 abs(f1(x1(n))-Lagr(x1210,y1210,x1(n)))];
end

figure
semilogy(x1,Delta15);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 5 узлов'})
grid minor
hold on 
semilogy(x1, Rn(d,x115,x1))
legend('экпериментальная','теоретическая')

figure
semilogy(x1,Delta18);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 8 узлов'})
grid minor
hold on 
semilogy(x1, Rn(d,x118,x1))
legend('экпериментальная','теоретическая')

figure
semilogy(x1,Delta110);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома';'Равномерная сетка, 10 узлов'})
grid minor
hold on 
semilogy(x1, Rn(d,x1110,x1))
legend('экпериментальная','теоретическая')

figure 
semilogy(x1,Delta115);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома ';'Чебышевкая сетка, 5 узлов'})
grid minor
hold on 

semilogy(x1,Rn(d,x125,x1))
legend('экпериментальная','теоретическая')
figure 
semilogy(x1,Delta118);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома ';'Чебышевкая сетка, 8 узлов'})
grid minor
hold on 

semilogy(x1,Rn(d,x128,x1))
legend('экпериментальная','теоретическая')

figure 
semilogy(x1,Delta1110);
xlabel('x')
ylabel('\delta')
title({'График поточечной ошибки полинома ';'Чебышевкая сетка, 10 узлов'})
grid minor
hold on 

semilogy(x1,Rn(d,x1210,x1))
legend('экпериментальная','теоретическая')






function [y] = Rn(D,X,x)
y = D(1.5);
    for i = 1:length(X)
        y = abs(y.*(x-X(i)));
    end
end
